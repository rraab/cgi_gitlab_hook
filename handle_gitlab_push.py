#!/usr/bin/env python3

import os
import sys
import cgi
import json

import cgitb
cgitb.enable()

import hashlib

import datetime as dt
from dateutil import tz

dirpath = os.path.dirname(os.path.realpath(__file__))

with open(os.path.join(dirpath, 'hook_salt.txt')) as f:
    hook_salt = f.read()

def salted_hash(string):
    return hashlib.sha256((string + hook_salt).encode()).hexdigest()[:16]

record_file = 'log.json'

def main():
    '''
    On push event, record:
    repo: {user: user, time: time}
    to 'namespace_push_log.json'
    '''

    now = dt.datetime.now(dt.timezone.utc)

    content_length = int(os.environ["CONTENT_LENGTH"])
    raw_data = sys.stdin.buffer.read(content_length).decode('utf-8')
    data = json.loads(raw_data)

    if not 'HTTP_X_GITLAB_TOKEN' in os.environ:

        try:
            if data['challenge'] == salted_hash('log'):

                print('Content-Type: application/json\n')
                try:
                    with open(record_file) as f:
                        print(f.read())
                        return
                except FileNotFoundError as e:
                    print('{}')
                    return

            else:
                print('Content-Type: text/plain\n')
                print('invalid challenge')
                return

        except KeyError as e:
            print('Content-Type: text/plain\n')
            print('no token header')
            return


    print('Content-Type: text/plain\n')

    try:
        after = data['after'] # last commit hash
    except KeyError as e:
        print('no "after" key received')
        return

    try:
        namespace = data['project']['namespace'] # hwX
    except KeyError as e:
        print('no project namespace received')
        return

    try:
        project_name = data['project']['name'] # cruz_id of project
    except KeyError as e:
        print('no project name received')
        return

    try:
        user_name = data['user_name'] # Actual Name
    except KeyError as e:
        print('no user_name received')
        return

    try:
        user_username = data['user_username'] # cruz_id of user
    except KeyError as e:
        print('no user_username received')
        return

    token = os.environ['HTTP_X_GITLAB_TOKEN']
    target_token = salted_hash(project_name)

    if not token == target_token:
        print('invalid token')
        return

    if os.path.exists(record_file):
        with open(os.path.join(dirpath, record_file)) as f:
            record = json.load(f)
    else:
        record = {}

    if namespace not in record:
        record[namespace] = {}

    if project_name not in record[namespace]:
        record[namespace][project_name] = []

    record[namespace][project_name].append({
        'user': user_username,
        'name': user_name,
        'time': now.ctime(),
        'hash': after
    })

    with open(os.path.join(dirpath, record_file), 'w') as f:
        json.dump(record, f, indent=2)

    print('OK')

if __name__ == "__main__":

    main()
