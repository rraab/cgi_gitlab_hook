#!/usr/bin/env python3

import os
import hashlib

if __name__ == '__main__':

    salt = hashlib.sha3_512(os.urandom(8)).hexdigest()[:16]

    with open('hook_salt.txt', 'w') as f:
        f.write(salt)

    print(salt)
