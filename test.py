#!/usr/bin/env python3

import requests
import json

import hashlib
import argparse

parser = argparse.ArgumentParser(description='Local testing of cgi-script')
parser.add_argument('--query', dest='query', action='store_true',)
args = parser.parse_args()

PORT = 8000

with open('hook_salt.txt') as f:
    hook_salt = f.read()

username = "user_cruz_id"
project_name = "project_cruz_id"
assignment_namespace = "hwX"

token = hashlib.sha256((project_name + hook_salt).encode()).hexdigest()[:16]
challenge = hashlib.sha256(('log' + hook_salt).encode()).hexdigest()[:16]

headers = {
    'Content-Type': 'application/json',
    'X-Gitlab-Token': token
}
data = {
    "user_username": 'Test Script',
    "user_name": username,
    "project": {
        "name": project_name,
        "namespace": assignment_namespace
    },
    "after": "xxx_last_commit_hash_xxx"
}

if args.query:
    r = requests.post(
        f'http://localhost:{PORT}/handle_gitlab_push.py',
        data=json.dumps({"challenge": challenge}),
        headers={'Content-Type': 'application/json'}
    )
else:
    r = requests.post(
        f'http://localhost:{PORT}/handle_gitlab_push.py',
        data=json.dumps(data),
        headers=headers
    )

print(r.content.decode())
