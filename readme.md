# GitLab Push-Logging with CGI Script

## Setup

1. Ensure you have an [soe.ucsc.edu account](https://accounts.soe.ucsc.edu/).

(typically used for webpages like https://users.soe.ucsc.edu/~your_soe_account_id)

2. ssh into your account:

        ssh your_soe_account_id@ssh.soe.ucsc.edu

3. I assume you haven't set up cgi scripts before. If you have, feel free to 
   adhere to whatever structure you prefer.
   
+ create directories, set permissions, clone this repository:

        mkdir -p ~/.html/cgi-bin
        cd ~/.html
        chmod 711 .
        cd cgi-bin
        chmod 711 .
        git clone https://git.ucsc.edu/rraab/cgi_gitlab_hook.git
        cd cgi_gitlab_hook
        chmod 711 .

4. generate a random salt to prevent students from abusing the
   handler (e.g., spoofing push events for classmates).

        python3 random_salt.py

+ copy the output of this script to a local file
+ save the url of the push handler
  e.g., http://users.soe.ucsc.edu/~your_soe_account_id/cgi-bin/cgi_gitlab_hook/handle_gitlab_push.py

## Use

* Refer to https://docs.gitlab.com/ee/user/project/integrations/webhooks.html

* With `python-gitlab`: 

        hook_token = hashlib.sha256((repo_name + hook_salt).encode()).hexdigest()[:16]

        hook = project.hooks.create({
            'url': cgi_hook_url,
            'token': hook_token,
            'push_events': 1
        })

* Refer to `test.py` for using `requests` to access logs

## Local Development 

`python3 local-cgi-serve.py`
    
In a second terminal:

`python3 test.py` To send fake push event
`python3 test.py --query` To get logs

